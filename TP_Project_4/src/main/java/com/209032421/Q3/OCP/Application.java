package com.209032421.Q3.OCP;

import com.209032421.Employee;
import com.209032421.Person;

public abstract class Application{

    public abstract Employee approveApplication(Person person);

}
