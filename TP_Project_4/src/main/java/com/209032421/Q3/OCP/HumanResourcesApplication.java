package com.209032421.Q3.OCP;

import com.209032421.Employee;
import com.209032421.Person;

public class HumanResourcesApplication extends Application{

    @Override
    public Employee approveApplication(Person person) {
        Employee newEmployee = new Employee(21, person.getFirstName(), person.getLastName(), person.getGender(), person.getAge(), 2500.00, "Human Resources Assistant", "HR Junior Assistant", 500, true);
        newEmployee.setEmployed(true);
        return newEmployee;
    }
}