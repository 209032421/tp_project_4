package com.209032421.Q3.SRP;

import com.209032421.Employee;

/**
 * Created by Bux_Life on 2017/03/30.
 */
public class EmployeeServices {

    public void updateSalary(Employee employee, double salary) {
        employee.setSalary(salary);
    }

    public boolean determineEmploymentStatus(Employee employee) {
        return employee.isEmployed();
    }
}


