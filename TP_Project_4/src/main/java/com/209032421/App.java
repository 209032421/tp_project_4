package com.209032421;

/**
 * 1. Write small program using TDD method to demostrate the three core principals of OOP.

 2. In (1) above, you wrote code demonstrating inheritance. Modify your code to use an alternative solution to inheritance.

 3. There are 7 core software principles and for each, write code that violate the principle and then write code that obeys the principle.

 4. Write code that violate ADP and also write code that corrects the violation.
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        System.out.println( "This Runs Question 1 and Question 2" );


        encapsulation();        //A demonstration of Encapsulation using get and set methods.
        polymorphism();         //A demonstration of Polymorphism, using a Person and Employee
        inheritance();          //A demonstration of Inheritance, Superclass Person, Subclass Employee
        composition();          //A demonstration of Composition(An alternative to concrete inheritance), using an object of type Employee and un-extended Class Duty
    }
}
