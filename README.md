Chapter 4 - Application Design Concepts and Principles
1. Write small program using TDD method to demostrate the three core principals of OOP.

2. In (1) above, you wrote code demonstrating inheritance. Modify your code to use an alternative solution to inheritance.

3. There are 7 core software principles and for each, write code that violate the principle and then write code that obeys the principle.

4. Write code that violate ADP and also write code that corrects the violation.

